//2
db.fruits.aggregate(
	[{$match: {onSale: true}}, 
        {$count: "onSale"}]
);

//3
db.fruits.aggregate(
    [{$match: {stock: {$gt: 20}}},
     {$count: "onSale"}]
);

//4
db.fruits.aggregate(
    [{$match: {onSale: true}},
    {$group: {
         _id: "$supplier_id",
         avg: {$avg: {$multiply: ["$price","$stock"]}}
       }}
     ]
);

//5
db.fruits.aggregate(
    [{$match: {onSale: true}},
    {$group: {
         _id: "$supplier_id",
         max: {$max: {$multiply: ["$price","$stock"]}}   
       }}
     ]
);

//6
db.fruits.aggregate(
    [{$match: {onSale: true}},
    {$group: {
         _id: "$supplier_id",
         min: {$min: {$multiply: ["$price","$stock"]}}
       }}
     ]
);